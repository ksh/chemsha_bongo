<h1>Chemsha Bongo - Quiz Competition</h1>

Project consists of two subprojects: Android App and C# Server.<br/>

<h2>Gameplay</h2>

The game is designed for 2, 4 or 8 players.<br/>
There are two game modes: Normal and Tournament. In Normal mode 2 players are given 10 questions and they have 5 seconds to choose an answer. Player with greater number of correctly answered questions wins. In Tournament mode bracket for 4 or 8 players is created. Players are divided into pairs and play a round just like in Normal mode. After that winners are divided into pairs and play the next round. Process is repeated until one tournament winner is emerged. Opponents are invited by the room host from his friend list. Players are awarded ranking points for each game they play.


<h2>Android App</h2>

Android App provides client functionality. User is able to create game room, invite friends and play with them by answering quiz questions. There is also a functionality that allows fetching player data such as profile, ranking and list of friends. In profile player can change his name by clicking on it. Inviting online and offline players by their name is supported.<br/>
The mockup can be viewed in Schemas/Client App Outline.jpg<br/>


<h2>Server Program</h2>

Server is responsible for responding to packets sent by client app. It assigns IDs to new players, sends necessary data such as player profiles, ranking and friend lists. It plays the main role in room administration and provides questions for players. It also manages player data and friend lists.<br/>


<h2>Directories</h2>

AndroidProject/ - contains Android App project<br/>

ChemshaBongoSERwer/ - contains C# server project<br/>

Schemas/ - contains outlines of client app, packet flow and transmission protocol<br/>