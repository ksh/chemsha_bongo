﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace ChemshaBongoSERwer
{
    public partial class CienSERwera : Form
    {
        public CienSERwera()
        {
            InitializeComponent();
        }

        Thread nasluchiwanie_na_glowyn_wejsciu;
        Thread GLOWNY;

        const int numer_protokolu = 18;

        List<Pokoj> lista_pokoi = new List<Pokoj>();
        List<Thread> pokoje = new List<Thread>();
        List<Thread> gniazda = new List<Thread>();
        List<string[]> lista_pytan = new List<string[]>();
        List<Gracz> lista_graczy = new List<Gracz>();
        List<List<int>> lista_przyjaciol = new List<List<int>>();
        delegate void Update0();
        delegate void Update1(string x);
        Queue<Komunikaty> lista_komunikatów = new Queue<Komunikaty>();
        
        int NASTEPNEID = 0;
        int NASTEPNEID_POKOJU = 0;

        void nasluchuj_na_porcie(object CLient)
        {
            Socket gniazdo = (Socket)CLient;
            while (true)
            {
                string dane = czytaj_komunikat(gniazdo, DateTime.Now.AddSeconds(5));
                if (dane != "")
                {
                    Komunikaty komunikat = new Komunikaty(dane);
                    komunikat.gniazdo_nadawcy = gniazdo;
                    lista_komunikatów.Enqueue(komunikat);
                    dodaj_do_logow("zakolejkowano komunikat do rozczytania");
                    if(komunikat.ident==0x43)
                    {
                        dodaj_do_logow("klient rozłączony: " + komunikat.id_nadawcy.ToString());
                        Thread.CurrentThread.Abort();
                    }
                }
            }
        }

        void odczytuj_komunikaty()
        {
            dodaj_do_logow("wątek główny wystartował");
            while(true)
            {
                if(lista_komunikatów.Count>0)
                {
                    Komunikaty komunikat = lista_komunikatów.Dequeue();
                    switch(komunikat.ident)
                    {
                        case 0x41://dodanie gracza
                            {
                                string komunikat_zwrotny = "B;";
                                if (komunikat.Dane[0] == "0")
                                {
                                    Gracz nowy_gracz = new Gracz();
                                    nowy_gracz.id = NASTEPNEID++;
                                    nowy_gracz.gniazdo = komunikat.gniazdo_nadawcy;
                                    nowy_gracz.online = true;
                                    nowy_gracz.nazwa = "Player" + nowy_gracz.id.ToString();
                                    lista_graczy.Add(nowy_gracz);
                                    dodaj_do_logow("dodano nowego gracza " + nowy_gracz.id.ToString());
                                    komunikat_zwrotny += "2;" + nowy_gracz.id.ToString();
                                }
                                else
                                {
                                    if (int.Parse(komunikat.Dane[1]) >= numer_protokolu)
                                    {
                                        int indeks = -1;
                                        for (int i = 0; i < lista_graczy.Count; ++i)
                                        {
                                            if (lista_graczy[i].id == int.Parse(komunikat.Dane[0]))
                                            {
                                                indeks = i;
                                                break;
                                            }
                                        }
                                        if (indeks != -1)
                                        {
                                            lista_graczy[indeks].gniazdo = komunikat.gniazdo_nadawcy;
                                            lista_graczy[indeks].online = true;
                                            dodaj_do_logow("zalogowano gracza " + lista_graczy[indeks].id.ToString());
                                        }
                                        else
                                        {
                                            Gracz gracz = new Gracz();
                                            gracz.id = int.Parse(komunikat.Dane[0]);
                                            gracz.gniazdo = komunikat.gniazdo_nadawcy;
                                            gracz.online = true;
                                            gracz.nazwa = "Player" + gracz.id.ToString();
                                            lista_graczy.Add(gracz);
                                            dodaj_do_logow("dopisano gracza " + gracz.id.ToString());
                                        }
                                        komunikat_zwrotny += "1";
                                    }
                                    else
                                    {
                                        komunikat_zwrotny += "0";
                                    }
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, komunikat_zwrotny);
                                break;
                            }
                        case 0x43://gracz rozłączony
                            {
                                int indeks = -1;
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if(lista_graczy[i].id == komunikat.id_nadawcy)
                                    {
                                        indeks = i;
                                        break;
                                    }
                                }
                                lista_graczy[indeks].online = false;
                                if (lista_graczy[indeks].id_pokoju > -1)
                                {
                                    lista_pokoi[lista_graczy[indeks].id_pokoju].gracze.Remove(lista_graczy[indeks]);
                                    lista_graczy[indeks].id_pokoju = -1;
                                }
                                dodaj_do_logow("Wylogowano gracza: " + komunikat.id_nadawcy.ToString());
                                break;
                            }
                        case 0x45://rezerwacja pokoju
                            {
                                string odpowiedz = "F;";
                                Pokoj pokoj = new Pokoj();
                                Gracz gracz = new Gracz();
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if(lista_graczy[i].id==komunikat.id_nadawcy)
                                    {
                                        gracz = lista_graczy[i];
                                        break;
                                    }
                                }
                                if (gracz.id_pokoju == -1)
                                {
                                    if (komunikat.Dane[0] == "1")
                                    {
                                        pokoj.tournament = true;
                                    }
                                    else
                                    {
                                        pokoj.tournament = false;
                                    }
                                    pokoj.id_pokoju = NASTEPNEID_POKOJU++;
                                    gracz.id_pokoju = pokoj.id_pokoju;
                                    pokoj.host_id = komunikat.id_nadawcy;
                                    pokoj.dodaj_gracza(gracz);

                                    lista_pokoi.Add(pokoj);
                                    odpowiedz += pokoj.id_pokoju.ToString();
                                    wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                    dodaj_do_logow("zarezerwowano pokoj: " + pokoj.id_pokoju.ToString() + " graczowi: " + komunikat.id_nadawcy.ToString());
                                    Thread watek_pokou = new Thread(new ParameterizedThreadStart(pokojf));
                                    watek_pokou.Start(pokoj);
                                    break;
                                }
                                else
                                {
                                    dodaj_do_logow("nie zarezerwowano pokoju graczowi: " + gracz.id.ToString() + " bo jest już w pokoju: " + gracz.id_pokoju.ToString());
                                    break;
                                }
                            }
                        case 0x47://zapytanie o przyjaciół online
                            {
                                string odpowiedz = "H;";
                                List<Gracz> lista = lista_przyjaciol_online_f(komunikat.id_nadawcy);
                                odpowiedz += lista.Count.ToString();
                                for (int i = 0; i < lista.Count; ++i)
                                {
                                    odpowiedz += ";" + lista[i].id.ToString() + ":" + lista[i].nazwa;
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                dodaj_do_logow("wysłano graczowi: " + komunikat.id_nadawcy.ToString() + " jego listę przyjaciół online");
                                break;
                            }
                        case 0x49:
                            {
                                string odpowiedz = "J;";
                                int indeks = -1;
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if(lista_graczy[i].id == int.Parse(komunikat.Dane[0]))
                                    {
                                        indeks = i;
                                        break;
                                    }
                                }
                                if(indeks >= 0)
                                {
                                    odpowiedz += lista_graczy[indeks].nazwa;
                                }
                                else
                                {
                                    odpowiedz += "Not Found";
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                break;
                            }
                        case 0x4B://zaproszenie gracza do pokoju
                            {
                                string zaproszenie = "M;";
                                zaproszenie += komunikat.id_nadawcy.ToString();
                                zaproszenie += ";" + komunikat.Dane[0];
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if(lista_graczy[i].id==int.Parse(komunikat.Dane[1]))
                                    {
                                        wyslij_komunikat(lista_graczy[i].gniazdo, zaproszenie);
                                        dodaj_do_logow("zaproszono gracza: " + lista_graczy[i].id.ToString() + " do pokoju: " + komunikat.Dane[0]);
                                        break;
                                    }
                                }
                                break;
                            }
                        case 0x4E://odpowiedź na zaproszenie do pokoju
                            {
                                string wiadomosc = "L;";
                                if(komunikat.Dane[1]=="1")
                                {
                                    for(int i = 0; i < lista_graczy.Count; ++i)
                                    {
                                        if (lista_graczy[i].id == komunikat.id_nadawcy)
                                        {
                                            Gracz gracz = lista_graczy[i];
                                            if (!lista_pokoi[int.Parse(komunikat.Dane[0])].gracze.Contains(gracz))
                                            {
                                                bool usunieto = true;
                                                try
                                                {
                                                    usunieto = lista_pokoi[gracz.id_pokoju].gracze.Remove(gracz);
                                                }
                                                catch
                                                {

                                                }
                                                if (usunieto)
                                                {
                                                    lista_pokoi[int.Parse(komunikat.Dane[0])].gracze.Add(lista_graczy[i]);
                                                    dodaj_do_logow("dodano gracza: " + lista_graczy[i].id.ToString() + " do pokoju:" + komunikat.Dane[0]);
                                                    gracz.id_pokoju = lista_pokoi[int.Parse(komunikat.Dane[0])].id_pokoju;
                                                }
                                            }
                                            else
                                            {

                                            }
                                            break;
                                        }
                                    }
                                }
                                wiadomosc += lista_pokoi[int.Parse(komunikat.Dane[0])].host_id.ToString() + ";" ;
                                wiadomosc += lista_pokoi[int.Parse(komunikat.Dane[0])].gracze.Count.ToString();
                                for (int i = 0; i < lista_pokoi[int.Parse(komunikat.Dane[0])].gracze.Count; ++i)
                                {
                                    wiadomosc += ";" + lista_pokoi[int.Parse(komunikat.Dane[0])].gracze[i].nazwa;
                                }
                                for (int i = 0; i < lista_pokoi[int.Parse(komunikat.Dane[0])].gracze.Count; ++i)
                                {
                                    wyslij_komunikat(lista_pokoi[int.Parse(komunikat.Dane[0])].gracze[i].gniazdo, wiadomosc);
                                }
                                break;
                            }
                        case 0x4F://początek gry
                            {
                                for(int i = 0; i < lista_pokoi.Count; ++i)
                                {
                                    if(lista_pokoi[i].id_pokoju == int.Parse(komunikat.Dane[0]))
                                    {
                                        string odpowiedz;
                                        if (!lista_pokoi[i].mozna_zaczynac && (!lista_pokoi[i].tournament || lista_pokoi[i].gracze.Count == 4 || lista_pokoi[i].gracze.Count == 8))
                                        {
                                            lista_pokoi[i].mozna_zaczynac = true;
                                            odpowiedz = "P;1;";
                                        }
                                        else
                                        {
                                            odpowiedz = "P;0;";
                                        }
                                        if (lista_pokoi[i].tournament)
                                        {
                                            odpowiedz += "1;";
                                            if(lista_pokoi[i].gracze.Count<=4)
                                            {
                                                odpowiedz += "1;";
                                                lista_pokoi[i].bracket_type = 1;
                                            }
                                            else
                                            {
                                                odpowiedz += "2;";
                                                lista_pokoi[i].bracket_type = 2;
                                            }
                                        }
                                        else
                                        {
                                            odpowiedz += "0;0;";
                                        }
                                        if (lista_pokoi[i].gracze.Count == 1)
                                        {
                                            odpowiedz += "2";
                                            for (int j = 0; j < lista_pokoi[i].gracze.Count; ++j)
                                            {
                                                odpowiedz += ";" + lista_pokoi[i].gracze[j].nazwa;
                                            }
                                            odpowiedz += ";∅";
                                        }
                                        else
                                        {
                                            odpowiedz += lista_pokoi[i].gracze.Count.ToString();
                                            for (int j = 0; j < lista_pokoi[i].gracze.Count; ++j)
                                            {
                                                odpowiedz += ";" + lista_pokoi[i].gracze[j].nazwa;
                                            }
                                        }
                                        
                                        for (int j = 0; j < lista_pokoi[i].gracze.Count; ++j)
                                        {
                                            wyslij_komunikat(lista_pokoi[i].gracze[j].gniazdo, odpowiedz);
                                        }
                                        break;
                                    }
                                }                                
                                break;
                            }
                        case 0x52://odpowiedx na pytanko
                            {
                                int indeks = -1;
                                for (int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if (lista_graczy[i].id == komunikat.id_nadawcy)
                                    {
                                        indeks = i;
                                        break;
                                    }
                                }
                                if (komunikat.Dane[1] == "1")
                                {
                                    lista_graczy[indeks].l_pkt++;
                                    lista_graczy[indeks].biezace_pkt++;
                                }
                                lista_graczy[indeks].odpowiedzial = true;
                                break;
                            }
                        case 0x55://zapytanie o profil
                            {
                                string odpowiedz = "V;";
                                int indeks = -1;
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if(lista_graczy[i].id==komunikat.id_nadawcy)
                                    {
                                        indeks = i;
                                        break;
                                    }
                                }
                                if (indeks >= 0)
                                {
                                    odpowiedz += lista_graczy[indeks].nazwa + ";" + lista_graczy[indeks].l_pkt.ToString() + ";" + lista_graczy[indeks].ranking.ToString() + ";" + lista_graczy[indeks].ilosc_gier.ToString() + ";" + lista_graczy[indeks].max_pkt.ToString();
                                }
                                else
                                {
                                    odpowiedz += "0;0;0;0;0";
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                break;
                            }
                        case 0x57://zmiana nazwy
                            {
                                string odpowiedz = "X;";
                                int indeks = -1;
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if(lista_graczy[i].id == komunikat.id_nadawcy)
                                    {
                                        indeks = i;
                                        break;
                                    }
                                }
                                if(indeks>=0)
                                {
                                    bool czyJuzJest = false;
                                    for(int j = 0; j < lista_graczy.Count; ++j)
                                    {
                                        if(lista_graczy[j].nazwa==komunikat.Dane[0]&&j!=indeks)
                                        {
                                            czyJuzJest = true;
                                            break;
                                        }
                                    }
                                    if (!czyJuzJest)
                                    {
                                        lista_graczy[indeks].nazwa = komunikat.Dane[0];
                                        odpowiedz += "1;" + lista_graczy[indeks].nazwa;
                                        dodaj_do_logow("zmieniona nazwę gracza: " + lista_graczy[indeks].id.ToString() + " na: " + lista_graczy[indeks].nazwa);
                                    }
                                    else
                                    {
                                        odpowiedz += "0;" + lista_graczy[indeks].nazwa;
                                        dodaj_do_logow("nie zmieniono nazwy: " + lista_graczy[indeks].id.ToString() + " bo juz jest: " + komunikat.Dane[0]);
                                    }
                                }
                                else
                                {
                                    odpowiedz += "0;" + komunikat.Dane[0];
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                break;
                            }
                        case 0x59://zapytanie o listę znajomych
                            {
                                string odpowiedz = "Z;";
                                List<Gracz> lista = lista_pzyjaciol_f(komunikat.id_nadawcy);
                                odpowiedz += lista.Count.ToString();
                                for(int i = 0; i < lista.Count; ++i)
                                {
                                    odpowiedz += ";" + lista[i].id.ToString() + ":" + lista[i].nazwa;
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                dodaj_do_logow("wysłano graczowi: " + komunikat.id_nadawcy.ToString() + " jego listę przyjaciół");
                                break;
                            }
                        case 0x61://zaproszenie przyajciela
                            {
                                string odpowiedz;
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if (lista_graczy[i].nazwa == komunikat.Dane[0])
                                    {
                                        odpowiedz = "d;";
                                        if (lista_graczy[i].online)
                                        {
                                            odpowiedz += komunikat.id_nadawcy.ToString() + ";";
                                            for (int j = 0; j < lista_graczy.Count; ++j)
                                            {
                                                if (lista_graczy[j].id == komunikat.id_nadawcy)
                                                {
                                                    odpowiedz += lista_graczy[j].nazwa;
                                                    wyslij_komunikat(lista_graczy[i].gniazdo, odpowiedz);
                                                    dodaj_do_logow("Wysłano graczowi zaproszenie");
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dodaj_do_logow("Gracz zapraszany offline");
                                            wyslij_komunikat(komunikat.gniazdo_nadawcy, "b;" + lista_graczy[i].id.ToString() + ";" + komunikat.Dane[0] + ";2");
                                        }                                        
                                        bool juzJest = false;
                                        for (int j = 0; j < lista_graczy[i].ListaZaproszen.Count; ++j)
                                        {
                                            if (lista_graczy[i].ListaZaproszen[j] == komunikat.id_nadawcy)
                                            {
                                                juzJest = true;
                                                break;
                                            }
                                        }
                                        if (!juzJest)
                                        {
                                            lista_graczy[i].ListaZaproszen.Add(komunikat.id_nadawcy);
                                        }
                                        int indeks = -1;
                                        for(int j = 0; j < lista_graczy.Count; ++j)
                                        {
                                            if(lista_graczy[j].id == komunikat.id_nadawcy)
                                            {
                                                indeks = j;
                                                break;
                                            }
                                        }
                                        juzJest = false;
                                        for (int j = 0; j < lista_graczy[indeks].ListaZaproszen_wychodzacych.Count; ++j)
                                        {
                                            if (lista_graczy[indeks].ListaZaproszen_wychodzacych[j] == lista_graczy[i].id)
                                            {
                                                juzJest = true;
                                                break;
                                            }
                                        }
                                        if (!juzJest)
                                        {
                                            lista_graczy[indeks].ListaZaproszen_wychodzacych.Add(lista_graczy[i].id);
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        case 0x65://odpowiedź na zaproszenie
                            {
                                string odpowiedz = "b;";
                                odpowiedz += komunikat.id_nadawcy + ";";
                                for(int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if(lista_graczy[i].id == komunikat.id_nadawcy)
                                    {
                                        odpowiedz += lista_graczy[i].nazwa + ";";
                                        break;
                                    }
                                }
                                odpowiedz += komunikat.Dane[1];
                                for (int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if (lista_graczy[i].id == int.Parse(komunikat.Dane[0]))
                                    {
                                        wyslij_komunikat(lista_graczy[i].gniazdo, odpowiedz);
                                        dodaj_do_logow("Wsyałno odpowiedź");
                                        break;
                                    }
                                }
                                if (komunikat.Dane[1] == "1" || komunikat.Dane[1] == "0")
                                {
                                    if(komunikat.Dane[1]=="1")
                                    {
                                        dodaj_przyjaciolx_y(komunikat.id_nadawcy, int.Parse(komunikat.Dane[0]));
                                    }
                                    List<int> nowa_lista = new List<int>(), nowa_lista_wychodzacych = new List<int>();
                                    for(int i = 0; i < lista_graczy.Count; ++i)
                                    {
                                        if(lista_graczy[i].id==komunikat.id_nadawcy)
                                        {
                                            for(int j = 0; j < lista_graczy[i].ListaZaproszen.Count;++j)
                                            {
                                                if(lista_graczy[i].ListaZaproszen[j]!=int.Parse(komunikat.Dane[0]))
                                                {
                                                    nowa_lista.Add(lista_graczy[i].ListaZaproszen[j]);
                                                }
                                            }
                                            lista_graczy[i].ListaZaproszen = nowa_lista;
                                            break;
                                        }
                                    }
                                    for (int i = 0; i < lista_graczy.Count; ++i)
                                    {
                                        if (lista_graczy[i].id == int.Parse(komunikat.Dane[0]))
                                        {
                                            for (int j = 0; j < lista_graczy[i].ListaZaproszen_wychodzacych.Count; ++j)
                                            {
                                                if (lista_graczy[i].ListaZaproszen_wychodzacych[j] != komunikat.id_nadawcy)
                                                {
                                                    nowa_lista_wychodzacych.Add(lista_graczy[i].ListaZaproszen_wychodzacych[j]);
                                                }
                                            }
                                            lista_graczy[i].ListaZaproszen_wychodzacych = nowa_lista_wychodzacych;
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        case 0x66://lista zaproszeń
                            {
                                string odpowiedz = "g;";
                                int indeks = -1;
                                for (int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if (lista_graczy[i].id == komunikat.id_nadawcy)
                                    {
                                        indeks = i;
                                        break;
                                    }
                                }
                                odpowiedz += lista_graczy[indeks].ListaZaproszen_wychodzacych.Count.ToString();
                                for(int i = 0; i < lista_graczy[indeks].ListaZaproszen_wychodzacych.Count; ++i)
                                {
                                    odpowiedz += ";" + lista_graczy[indeks].ListaZaproszen_wychodzacych[i].ToString() + ":";
                                    for(int j = 0; j < lista_graczy.Count; ++j)
                                    {
                                        if(lista_graczy[j].id == lista_graczy[indeks].ListaZaproszen_wychodzacych[i])
                                        {
                                            odpowiedz += lista_graczy[j].nazwa;
                                        }
                                    }
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                dodaj_do_logow("Wysałno listę zaproszeń graczowi: " + komunikat.id_nadawcy.ToString());
                                break;
                            }
                        case 0x68://lista zaproszeń otrzymanych
                            {
                                string odpowiedz = "i;";
                                int indeks = -1;
                                for (int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    if (lista_graczy[i].id == komunikat.id_nadawcy)
                                    {
                                        indeks = i;
                                        break;
                                    }
                                }
                                odpowiedz += lista_graczy[indeks].ListaZaproszen.Count.ToString();
                                for (int i = 0; i < lista_graczy[indeks].ListaZaproszen.Count; ++i)
                                {
                                    odpowiedz += ";" + lista_graczy[indeks].ListaZaproszen[i].ToString() + ":";
                                    for (int j = 0; j < lista_graczy.Count; ++j)
                                    {
                                        if (lista_graczy[j].id == lista_graczy[indeks].ListaZaproszen[i])
                                        {
                                            odpowiedz += lista_graczy[j].nazwa;
                                        }
                                    }
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                dodaj_do_logow("Wysałno listę zaproszeń otrzymanych graczowi: " + komunikat.id_nadawcy.ToString());
                                break;
                            }
                        case 0x6A://ranking
                            {
                                aktualizuj_ranking(lista_graczy);
                                string odpowiedz = "k;";
                                odpowiedz += lista_graczy.Count.ToString();
                                for (int i = 0; i < lista_graczy.Count; ++i)
                                {
                                    odpowiedz += ";" + lista_graczy[i].nazwa + ":" + lista_graczy[i].l_pkt.ToString() + ":" + lista_graczy[i].ranking.ToString();
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                dodaj_do_logow("wysłano graczowi: " + komunikat.id_nadawcy.ToString() + " listę rankingową");
                                break;
                            }
                        case 0x6C://usunęcie przyajciela
                            {
                                string odpowiedz = "m;";
                                odpowiedz += komunikat.Dane[0] + ";";
                                if (usun_przyjaciolx_y(komunikat.id_nadawcy, int.Parse(komunikat.Dane[0])))
                                {
                                    odpowiedz += "1";
                                }
                                else
                                {
                                    odpowiedz += "0";
                                }
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, odpowiedz);
                                break;
                            }
                        default:
                            {
                                dodaj_do_logow("odsyłam komunikat nadawcy");
                                wyslij_komunikat(komunikat.gniazdo_nadawcy, komunikat.oryginalna_tresc);
                                break;
                            }
                    }
                }
                else
                {

                }
            }
        }

        void Zacznij_nasluchiwac()
        {
            dodaj_do_logow("wątek nasłuchu wystartował");
            lista_komunikatów = new Queue<Komunikaty>();
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 1415);
            TcpListener SERver = new TcpListener(localEndPoint);
            SERver.Start();
            pisz_w_txtBox(localEndPoint.ToString());
            try
            {
                dodaj_do_logow("Czekanie na połączenia...");
                while (true)
                {
                    try
                    {
                        Socket Client;
                        //Client  = serverSocket.Accept();
                        if (SERver.Pending())
                        {

                            Client = SERver.AcceptSocket();
                            dodaj_do_logow("Połączono z " + Client.RemoteEndPoint);
                            Thread nasluchiwanie = new Thread(new ParameterizedThreadStart(nasluchuj_na_porcie));
                            nasluchiwanie.Start(Client);
                            gniazda.Add(nasluchiwanie);
                        }
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception e)
            {
                dodaj_do_logow(e.ToString());
            }
            
        }

        void wyslij_komunikat(Socket gniazdo, string komunikat)
        {
            byte[] bajty = Encoding.ASCII.GetBytes(komunikat + "\n");
            gniazdo.Send(bajty);
            dodaj_do_logow("Wysłano: " + komunikat + " do: " + gniazdo.RemoteEndPoint.ToString());
        }

        string czytaj_komunikat(Socket gniazdo, DateTime limit_czasowy)
        {
            string wynik = "";
            while(DateTime.Now<=limit_czasowy)
            {
                try
                {
                    byte[] bajty = new byte[1024];
                    int ilosc_bajtow = gniazdo.Receive(bajty);
                    wynik += Encoding.ASCII.GetString(bajty, 0, ilosc_bajtow);
                    if (wynik.Substring(wynik.Length-1)=="\n")
                    {
                        wynik = wynik.Substring(0, wynik.Length - 1);
                        dodaj_do_logow("Odebrano : " + wynik + " od: " + gniazdo.RemoteEndPoint.ToString());
                        return wynik;
                    }
                }
                catch
                {

                }
            }
            //dodaj_do_logow("nie odebrano nic od: " + gniazdo.RemoteEndPoint.ToString());
            return wynik;
        }


        void dodaj_do_logow(string tekst)//dosyć ważna funkcja dodająca cokolwiek do logow richtextboxie i do pliku
        {
            
            if(this.InvokeRequired)
            {
                try
                {
                    Invoke(new Update1(dodaj_do_logow), new object[] { tekst });
                }
                catch
                {

                }
                
            }
            else
            {
                tekst = DateTime.Now.ToString() + ";" + tekst;
                try
                {
                    richTextBox1.AppendText(tekst + "\n");
                }
                catch
                {

                }
                try
                {
                    File.AppendAllText("C://KSH/log.csv", tekst + "\n");
                }
                catch
                {

                }
            }
        }

        void pisz_w_txtBox(string x)
        {
            if(this.InvokeRequired)
            {
                Invoke(new Update1(pisz_w_txtBox), new object[] { x });
            }
            else
            {
                textBox1.Text = x;
            }
        }

        private void Form1_Load(object sender, EventArgs e)//funkcja wywoływana przy starcie programu
        {
            richTextBox1.Text = "Podgląd logu na żywo:\n";
            if(!Directory.Exists("C://KSH"))
            {
                Directory.CreateDirectory("C://KSH");
            }
            if(!File.Exists("C://KSH/log.csv"))
            {
                File.AppendAllText("C://KSH/log.csv", "A tu będą zapisywane wszystkie logi z serwera.\n");
            }
            dodaj_do_logow("");
            dodaj_do_logow("");
            dodaj_do_logow("Start SERwera");
            if (!File.Exists("C://KSH/gracze.csv"))
            {
                File.AppendAllText("C://KSH/gracze.csv","ID;Nazwa;Pkt;Ranking;ilość gier; max pkt");
            }
            else
            {
                string[] pom = null;
                try
                {
                    pom = File.ReadAllLines("C://KSH/gracze.csv");
                    if (pom == null)
                    {
                        lista_graczy = null;
                    }
                    else
                    {
                        for (int i=1; i<pom.Length;++i)
                        {
                            string[] linia = pom[i].Split(';');
                            Gracz gracz = new Gracz();
                            gracz.id = int.Parse(linia[0]);
                            gracz.nazwa = linia[1];
                            gracz.l_pkt = int.Parse(linia[2]);
                            gracz.ranking = int.Parse(linia[3]);
                            gracz.ilosc_gier = int.Parse(linia[4]);
                            gracz.max_pkt = int.Parse(linia[5]);
                            lista_graczy.Add(gracz);
                        }
                    }
                    dodaj_do_logow("Wczytano listę graczy");
                }
                catch
                {
                    MessageBox.Show("Błąd odczytu");
                    this.Close();
                }
            }
            if (!File.Exists("C://KSH/lista_przyjaciol.csv"))
            {
                File.Create("C://KSH/lista_przyjaciol.csv");
            }
            else
            {
                string[] pom = null;
                try
                {
                    pom = File.ReadAllLines("C://KSH/lista_przyjaciol.csv");
                    if (pom == null)
                    {
                        lista_graczy = null;
                    }
                    else
                    {
                        for (int i = 0; i < pom.Length; ++i)
                        {
                            List<int> lista_pom = new List<int>();
                            string[] linia = pom[i].Split(';');
                            for (int j = 0; j < linia.Length; ++j)
                            {
                                lista_pom.Add(int.Parse(linia[j]));
                            }
                            lista_przyjaciol.Add(lista_pom);
                        }
                    }
                    dodaj_do_logow("Wczytano listę przyjaciół");
                }
                catch
                {
                    MessageBox.Show("Błąd odczytu");
                    this.Close();
                }
            }
            if(!File.Exists("C://KSH/baza_pytan.csv"))
            {
                MessageBox.Show("Brak dostępnych pytań");
                this.Close();
            }
            else
            {
                string[] pom = null;
                try
                {
                    pom = File.ReadAllLines("C://KSH/baza_pytan.csv");
                    if (pom == null)
                    {
                        MessageBox.Show("Problem z plikiem");
                        this.Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Problem z plikiem");
                    this.Close();
                }
                //lista_pytan = new string[pom.Length, 5];
                for (int i = 1; i < pom.Length; ++i)
                {
                    string[] linia = pom[i].Split(';');
                    lista_pytan.Add(linia);
                    /*for(int j = 0; j < 5; j++)
                    {
                        lista_pytan[i, j] = linia[j];
                    }*/
                }
                dodaj_do_logow("Wczytano liste pytań");                
            }


            /*for (int i = 0; i < lista_pytan.Count; ++i)
            {
                string linia = "";
                for (int j = 0; j < lista_pytan[i].Length; ++j)
                {
                    linia += lista_pytan[i][j] + " ";
                }
                richTextBox3.AppendText(linia + "\n");
            }*/
            for (int i = 0; i < lista_graczy.Count; ++i)
            {
                string linia = "";
                for (int j = 0; j < 4; ++j)
                {
                    linia = lista_graczy[i].id.ToString() + " " + lista_graczy[i].nazwa + " " + lista_graczy[i].l_pkt.ToString() + " " + lista_graczy[i].ranking.ToString() + " " + lista_graczy[i].ilosc_gier.ToString() + " " + lista_graczy[i].max_pkt.ToString();
                }
                richTextBox3.AppendText(linia + "\n");
            }
            for (int i = 0; i < lista_przyjaciol.Count; ++i)
            {
                string linia = "";
                for (int j = 0; j < lista_przyjaciol[i].Count; ++j)
                {
                    linia += lista_przyjaciol[i][j] + " ";
                }
                richTextBox3.AppendText(linia + "\n");
            }

            for (int i = 0; i < lista_graczy.Count; ++i)
            {
                if(lista_graczy[i].id>NASTEPNEID)
                {
                    NASTEPNEID = lista_graczy[i].id;
                }
            }
            ++NASTEPNEID;
        }

        void pokojf(object arg)
        {
            Pokoj pokoj = (Pokoj)arg;
            pokoje.Add(Thread.CurrentThread);
            pokoj.numer_watku = pokoje.Count - 1;
            if (pokoj.wszyscy_gracze.Count == 0)
            {
                pokoj.wszyscy_gracze = pokoj.gracze;
            }
            lista_pokoi.Add(pokoj);
            while (!pokoj.mozna_zaczynac)
            {
                if (pokoj.gracze.Count == 0)
                {
                    dodaj_do_logow("zamykanie pokoju nr: " + pokoj.id_pokoju.ToString() + " z powodu braku graczy");
                    Thread.CurrentThread.Abort();
                }
            }
            rozpoczecie_gry_w_pokoju(pokoj);
            bool czy_koniec = false;
            for (int i = 0; i < 10 && !czy_koniec; ++i)
            {
                bool czy_mozna_kolejne_pytanie = true;
                for (int j = 0; j < pokoj.gracze.Count; ++j)
                {
                    string komunikat = "Q;" + i.ToString();
                    for (int k = 0; k < 5; ++k)
                    {
                        komunikat += ";" + pokoj.pytania[i, k];
                    }
                    pokoj.gracze[j].odpowiedzial = false;
                    wyslij_komunikat(pokoj.gracze[j].gniazdo, komunikat);
                    dodaj_do_logow("Wysłano graczowi: " + pokoj.gracze[j].id.ToString() + " pytanie: " + pokoj.pytania[i, 0]);
                }
                do
                {
                    czy_mozna_kolejne_pytanie = true;
                    for (int j = 0; j < pokoj.gracze.Count; ++j)
                    {
                        czy_mozna_kolejne_pytanie &= pokoj.gracze[j].odpowiedzial;
                        if(pokoj.gracze.Count==0)
                        {
                            dodaj_do_logow("zamykanie pokoju nr: " + pokoj.id_pokoju.ToString() + " z powodu braku graczy");
                            Thread.CurrentThread.Abort();
                        }
                    }
                }
                while (!czy_mozna_kolejne_pytanie);
            }
            zamkniecie_pokoju(pokoj);
            if(pokoj.tournament && pokoj.gracze.Count>2)
            {
                Pokoj pokoj_cd = new Pokoj();
                pokoj_cd.id_pokoju = NASTEPNEID_POKOJU++;
                pokoj_cd.host_id = pokoj.gracze[0].id;
                pokoj_cd.tournament = true;
                pokoj_cd.bracket_type = pokoj.bracket_type;
                int ilosc_graczy = int.Parse(Math.Ceiling(Math.Log(pokoj.gracze.Count, 2)).ToString());
                pokoj_cd.wszyscy_gracze = pokoj.wszyscy_gracze;
                for(int i = 0; i < ilosc_graczy; ++i)
                {
                    pokoj_cd.wszyscy_gracze.Add(pokoj.gracze[i]);
                    pokoj_cd.gracze.Add(pokoj.gracze[i]);
                }                
                pokoj_cd.mozna_zaczynac = true;
                generuj_Game_State(pokoj_cd);
                Thread watek_pokou = new Thread(new ParameterizedThreadStart(pokojf));
                watek_pokou.Start(pokoj_cd);
            }
            Thread.CurrentThread.Abort();
        }

        void generuj_Game_State(Pokoj pokoj)
        {
            string komunikat = "n;1;";
            komunikat += pokoj.bracket_type.ToString() + ";";
            komunikat += pokoj.wszyscy_gracze.Count.ToString();
            for(int i = 0; i < pokoj.wszyscy_gracze.Count; ++i)
            {
                komunikat += ";" + pokoj.wszyscy_gracze[i].nazwa;
            }
            for(int i = 0; i < pokoj.gracze.Count; ++i)
            {
                wyslij_komunikat(pokoj.gracze[i].gniazdo, komunikat);
            }
        }

        void dodaj_przyjaciolx_y(int x, int y)
        {
            if (lista_przyjaciol == null)
            {
                List<int> pom = new List<int>();
                pom.Add(x);
                pom.Add(y);
                lista_przyjaciol.Add(pom);
                pom = new List<int>();
                pom.Add(y);
                pom.Add(x);
                lista_przyjaciol.Add(pom);
            }
            else
            {
                int indeks = -1;
                for (int i = 0; i < lista_przyjaciol.Count; ++i)
                {
                    if (lista_przyjaciol[i][0] == x)
                    {
                        indeks = i;
                        break;
                    }
                }
                if (indeks == -1)
                {
                    List<int> pom = new List<int>();
                    pom.Add(x);
                    pom.Add(y);
                    lista_przyjaciol.Add(pom);
                }
                else
                {
                    bool juz_jest = false;
                    for (int i = 0; i < lista_przyjaciol[indeks].Count; ++i)
                    {
                        if (lista_przyjaciol[indeks][i] == y)
                        {
                            juz_jest = true;
                            break;
                        }
                    }
                    if (!juz_jest)
                    {
                        lista_przyjaciol[indeks].Add(y);
                    }
                }
                indeks = -1;
                for (int i = 0; i < lista_przyjaciol.Count; ++i)
                {
                    if (lista_przyjaciol[i][0] == y)
                    {
                        indeks = i;
                        break;
                    }
                }
                if (indeks == -1)
                {
                    List<int> pom = new List<int>();
                    pom.Add(y);
                    pom.Add(x);
                    lista_przyjaciol.Add(pom);
                }
                else
                {
                    bool juz_jest = false;
                    for (int i = 0; i < lista_przyjaciol[indeks].Count; ++i)
                    {
                        if (lista_przyjaciol[indeks][i] == x)
                        {
                            juz_jest = true;
                            break;
                        }
                    }
                    if (!juz_jest)
                    {
                        lista_przyjaciol[indeks].Add(x);
                    }
                }
            }
            dodaj_do_logow("dodano parę przyjaciół : " + x.ToString() + " - " + y.ToString());
            zapisz_pliki();
        }

        bool usun_przyjaciolx_y(int x, int y)
        {
            bool wynik = true;
            int indeks = -1;
            for(int i = 0; i < lista_przyjaciol.Count; ++i)
            {
                if(lista_przyjaciol[i][0]==x)
                {
                    wynik &= lista_przyjaciol[i].Remove(y);
                }
            }
            indeks = -1;
            for (int i = 0; i < lista_przyjaciol.Count; ++i)
            {
                if (lista_przyjaciol[i][0] == y)
                {
                    wynik &= lista_przyjaciol[i].Remove(x);
                }
            }
            zapisz_pliki();
            return wynik;
        }

        List<Gracz> lista_pzyjaciol_f(int id_gracza)
        {
            List<Gracz> wynik = new List<Gracz>();
            for(int i = 0; i < lista_przyjaciol.Count;++i)
            {
                if(lista_przyjaciol[i][0]==id_gracza)
                {                    
                    for(int j = 1; j < lista_przyjaciol[i].Count;++j)
                    {
                        int indeks = -1;
                        for(int k = 0; k < lista_graczy.Count; ++k)
                        {
                            if(lista_graczy[k].id==lista_przyjaciol[i][j])
                            {
                                indeks = k;
                                break;
                            }
                        }
                        if (indeks >= 0)
                        {
                            wynik.Add(lista_graczy[indeks]);
                        }
                    }
                    break;
                }
            }
            return wynik;
        }

        List<Gracz> lista_przyjaciol_online_f(int id_gracza)
        {
            List<Gracz> wynik = new List<Gracz>();
            for (int i = 0; i < lista_przyjaciol.Count; ++i)
            {
                if (lista_przyjaciol[i][0] == id_gracza)
                {
                    for (int j = 1; j < lista_przyjaciol[i].Count; ++j)
                    {
                        int indeks = -1;
                        for (int k = 0; k < lista_graczy.Count; ++k)
                        {
                            if (lista_graczy[k].id == lista_przyjaciol[i][j])
                            {
                                indeks = k;
                                break;
                            }
                        }
                        if (indeks >= 0 && lista_graczy[indeks].online)
                        {
                            wynik.Add(lista_graczy[indeks]);
                        }
                    }
                    break;
                }
            }
            return wynik;
        }

        void rozpoczecie_gry_w_pokoju(Pokoj pokoj)
        {
            Random x = new Random();
            int[] lista_id_pytan = new int[10];
            for(int i = 0; i < 10; ++i)
            {
                int y = x.Next() % lista_pytan.Count;
                bool pytanie_juz_jest;
                do
                {
                    pytanie_juz_jest = false;
                    for (int j = 0; j < i; ++j)
                    {
                        if (lista_id_pytan[j] == y)
                        {
                            pytanie_juz_jest = true;
                            y = (y + 1) % lista_pytan.Count;
                            break;
                        }
                    }
                }
                while (pytanie_juz_jest);
                lista_id_pytan[i] = y;
                for(int j = 0; j < 5; ++j)
                {
                    pokoj.pytania[i, j] = lista_pytan[y][j];
                }
            }
            for(int i = 0; i < pokoj.gracze.Count; ++i)
            {
                pokoj.gracze[i].id_pokoju = pokoj.id_pokoju;
                pokoj.gracze[i].biezace_pkt = 0;
            }
            dodaj_do_logow("rozpoczęto grę w pokoju: " + pokoj.id_pokoju.ToString());
        }

        void zamkniecie_pokoju(Pokoj pokoj)
        {
            for (int i = 0; i < pokoj.gracze.Count; ++i)//sortowanie
            {
                for (int j = 0; j < pokoj.gracze.Count - i - 1; ++j)
                {
                    if (pokoj.gracze[j + 1].biezace_pkt > pokoj.gracze[j].biezace_pkt)
                    {
                        swap(pokoj.gracze, j, j + 1);
                    }
                }
            }
            pokoj.gracze[0].biezace_pkt += pokoj.gracze.Count - 1;
            pokoj.gracze[0].l_pkt += pokoj.gracze.Count - 1;
            for (int i = 0; i < pokoj.gracze.Count; ++i)
            {
                pokoj.gracze[i].id_pokoju = -1;
                if (pokoj.gracze[i].biezace_pkt > pokoj.gracze[i].max_pkt)
                {
                    pokoj.gracze[i].max_pkt = pokoj.gracze[i].biezace_pkt;
                }
                string komunikat = "T;";
                komunikat += pokoj.gracze[i].biezace_pkt.ToString() + ";" + pokoj.gracze.Count.ToString();
                for(int j = 0; j < pokoj.gracze.Count; ++j)
                {
                    komunikat += ";" + pokoj.gracze[j].nazwa;
                }
                wyslij_komunikat(pokoj.gracze[i].gniazdo, komunikat);
            }
            zapisz_pliki();
            dodaj_do_logow("Pokoj: " + pokoj.id_pokoju.ToString() + " zamknięty");
            aktualizuj_ranking(lista_graczy);
        }

        private void button1_Click(object sender, EventArgs e)//restart
        {
            //jakiś kod
            //wyłaczenie wszystkich pokoi
            zamknij_SERwer();
            dodaj_do_logow("restart");
            Thread.Sleep(100);
            Application.Restart();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            nasluchiwanie_na_glowyn_wejsciu = new Thread(Zacznij_nasluchiwac);
            nasluchiwanie_na_glowyn_wejsciu.Start();
            GLOWNY = new Thread(odczytuj_komunikaty);
            GLOWNY.Start();
        }

        void zamknij_SERwer()
        {
            zapisz_pliki();
            for(int i = 0; i < gniazda.Count; ++i)
            {
                gniazda[i].Abort();
            }
            for (int i = 0; i < pokoje.Count; ++i)
            {
                pokoje[i].Abort();
            }
            try
            {
                nasluchiwanie_na_glowyn_wejsciu.Abort();
                GLOWNY.Abort();
            }
            catch
            {

            }
            for(int i = 0; i < lista_graczy.Count; ++i)
            {
                if(lista_graczy[i].online)
                {
                    wyslij_komunikat(lista_graczy[i].gniazdo, "D");
                }
            }
        }

        private void CienSERwera_FormClosing(object sender, FormClosingEventArgs e)
        {
            zamknij_SERwer();
            dodaj_do_logow("zamykanie serwera");
            Thread.Sleep(100);
        }

        void zapisz_pliki()
        {
            string[] linie;
            string linia;
            List<string> linie_;
            linie_ = new List<string>();
            linia = "ID;Nazwa;Pkt;Ranking;ilość gier; max pkt";
            linie_.Add(linia);
            for (int i = 0; i < lista_graczy.Count; ++i)
            {
                linia = lista_graczy[i].id.ToString() + ";" + lista_graczy[i].nazwa + ";" + lista_graczy[i].l_pkt.ToString() + ";" + lista_graczy[i].ranking.ToString() + ";" + lista_graczy[i].ilosc_gier.ToString() + ";" + lista_graczy[i].max_pkt.ToString();
                linie_.Add(linia);
            }
            linie = lista_do_tabeli(linie_);
            File.WriteAllLines("C://KSH/gracze.csv", linie);
            dodaj_do_logow("zapisano plik z graczami");
            linie_ = new List<string>();
            for (int i = 0; i < lista_przyjaciol.Count; ++i)
            {
                linia = lista_przyjaciol[i][0].ToString();
                for(int j = 1; j < lista_przyjaciol[i].Count; ++j)
                {
                    linia += ";" + lista_przyjaciol[i][j].ToString();
                }
                linie_.Add(linia);
            }
            linie = lista_do_tabeli(linie_);
            File.WriteAllLines("C://KSH/lista_przyjaciol.csv", linie);
            dodaj_do_logow("zapisano plik z przyjaciółmi");
        }

        string[] lista_do_tabeli(List<string> lista)
        {
            string[] tabela = new string[lista.Count];
            for(int i = 0; i < tabela.Length; ++i)
            {
                tabela[i] = lista[i];
            }
            return tabela;
        }

        void swap(List<Gracz> T, int indeks1, int indeks2)
        {
            Gracz temp = T[indeks1];
            T[indeks1] = T[indeks2];
            T[indeks2] = temp;
        }

        void quick_sort(List<Gracz> T, int p, int k)
        {
            if ((k - p) > 1)
            {
                int s = (p + k) / 2;
                int pivot = T[s].l_pkt;
                int i = p, j = k;
                while(i<j)
                {
                    if(T[i].l_pkt<=pivot)
                    {
                        while(T[j].l_pkt<pivot)
                        {
                            --j;
                        }
                        swap(T, i++, j--);
                    }
                    else
                    {
                        ++i;
                    }
                }
                quick_sort(T, p, i - 1);
                quick_sort(T, i, k);
            }
            else
            {
                if(T[p].l_pkt<T[k].l_pkt)
                {
                    swap(T, p, k);
                }
            }
        }

        void aktualizuj_ranking(List<Gracz> T)
        {
            quick_sort(T, 0, T.Count - 1);
            for(int i = 0; i < T.Count; ++i)
            {
                T[i].ranking = i;
            }
            dodaj_do_logow("zaktualizowano ranking");
            zapisz_pliki();
        }

        private void button2_Click(object sender, EventArgs e)//ręczne zamknięcie
        {
            zamknij_SERwer();
            dodaj_do_logow("zamykanie serwera");
            Thread.Sleep(100);
            this.Close();
        }
    }

    class Komunikaty
    {
        public Komunikaty(string x)//konstruktor klasy
        {
            oryginalna_tresc = x;
            string[] pom = x.Split(';');
            switch (pom[0])
            {
                case "A":
                    {
                        ident = 0x41;
                        Dane = new string[2];
                        Dane[0] = pom[1];
                        Dane[1] = pom[2];
                        break;
                    }
                case "C":
                    {
                        ident = 0x43;
                        Dane = null;
                        id_nadawcy = int.Parse(pom[1]);
                        break;
                    }
                case "E":
                    {
                        ident = 0x45;
                        id_nadawcy = int.Parse(pom[1]);
                        Dane = new string[1];
                        Dane[0] = pom[2];
                        break;
                    }
                case "G":
                    {
                        ident = 0x47;
                        Dane = null;
                        id_nadawcy = int.Parse(pom[1]);
                        break;
                    }
                case "I":
                    {
                        ident = 0x49;
                        Dane = new string[1];
                        Dane[0] = pom[1];
                        break;
                    }
                case "K":
                    {
                        ident = 0x4B;
                        id_nadawcy = int.Parse(pom[1]);
                        Dane = new string[2];
                        Dane[0] = pom[2];
                        Dane[1] = pom[3];
                        break;
                    }
                case "N":
                    {
                        ident = 0x4E;
                        id_nadawcy = int.Parse(pom[1]);
                        Dane = new string[2];
                        Dane[0] = pom[2];
                        Dane[1] = pom[3];
                        break;
                    }
                case "O":
                    {
                        ident = 0x4F;
                        Dane = new string[1];
                        Dane[0] = pom[1];
                        break;
                    }
                case "R":
                    {
                        ident = 0x52;
                        id_nadawcy = int.Parse(pom[1]);
                        Dane = new string[2];
                        Dane[0] = pom[2];
                        Dane[1] = pom[3];
                        break;
                    }
                case "U":
                    {
                        ident = 0x55;
                        Dane = null;
                        id_nadawcy = int.Parse(pom[1]);
                        break;
                    }
                case "W":
                    {
                        ident = 0x57;
                        Dane = new string[1];
                        id_nadawcy = int.Parse(pom[1]);
                        Dane[0] = pom[2];
                        break;
                    }
                case "Y":
                    {
                        ident = 0x59;
                        Dane = null;
                        id_nadawcy = int.Parse(pom[1]);
                        break;
                    }
                case "a":
                    {
                        ident = 0x61;
                        Dane = new string[1];
                        id_nadawcy = int.Parse(pom[1]);
                        Dane[0] = pom[2];
                        break;
                    }
                case "e":
                    {
                        ident = 0x65;
                        id_nadawcy = int.Parse(pom[1]);
                        Dane = new string[2];
                        Dane[0] = pom[2];
                        Dane[1] = pom[3];
                        break;
                    }
                case "f":
                    {
                        ident = 0x66;
                        Dane = null;
                        id_nadawcy = int.Parse(pom[1]);
                        break;
                    }
                case "h":
                    {
                        ident = 0x68;
                        Dane = null;
                        id_nadawcy = int.Parse(pom[1]);
                        break;
                    }
                case "j":
                    {
                        ident = 0x6A;
                        Dane = null;
                        id_nadawcy = int.Parse(pom[1]);
                        break;
                    }
                case "l":
                    {
                        ident = 0x6C;
                        Dane = new string[1];
                        id_nadawcy = int.Parse(pom[1]);
                        Dane[0] = pom[2];
                        break;
                    }
                default:
                    {
                        ident = -1;
                        break;
                    }
            }
        }

        public string oryginalna_tresc = "";
        public Socket gniazdo_nadawcy = null;
        public int id_nadawcy = -1;
        public string[] Dane = null;
        public int ident = -1;
    }

    class Pokoj
    {
        public Pokoj()
        {

        }


        public void dodaj_gracza(Gracz gracz)
        {
            gracze.Add(gracz);
            gracz.ilosc_gier++;
        }

        public int bracket_type = 0;
        public List<Gracz> wszyscy_gracze = new List<Gracz>();
        public int host_id;
        public bool mozna_zaczynac = false;
        public bool tournament = false;
        public string[,] pytania = new string[10, 5];
        public int id_pokoju;
        public List<Gracz> gracze = new List<Gracz>();
        public int numer_watku;
    }

    class Gracz
    {
        public Gracz()
        {
            ListaZaproszen = new List<int>();
            ListaZaproszen_wychodzacych = new List<int>();
        }

        public int id_pokoju = -1;
        public bool odpowiedzial = true;
        public int id = -1;
        public string nazwa = "";
        public int l_pkt = 0;
        public int biezace_pkt = 0;
        public int ranking = -1;
        public int ilosc_gier = 0;
        public int max_pkt = 0;
        public bool online = false;
        //public bool spectate = false; //a co, możemy!
        public Socket gniazdo;
        public List<int> ListaZaproszen;
        public List<int> ListaZaproszen_wychodzacych;
    }
}
