package com.ksh.chemshabongo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    Messenger connectionManagerMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");

        Bundle b = new Bundle();
        b.putInt("number", 85);
        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, ProfileActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();

                if(b.getInt("number") == 86) {
                    TextView nameTV = (TextView) findViewById(R.id.player_name);
                    String name = b.getString("name");
                    nameTV.setText(name);
                    nameTV.setClickable(true);

                    TextView pointsTV = (TextView) findViewById(R.id.player_points);
                    int points = b.getInt("points");
                    pointsTV.setText(Integer.toString(points));

                    TextView rankingTV = (TextView) findViewById(R.id.player_ranking);
                    int ranking = b.getInt("ranking");
                    rankingTV.setText(Integer.toString(ranking));

                    TextView gamesPlayedTV = (TextView) findViewById(R.id.player_games_played);
                    int gamesPlayed = b.getInt("gamesPlayed");
                    gamesPlayedTV.setText(Integer.toString(gamesPlayed));

                    TextView maxPointsEarnTV = (TextView) findViewById(R.id.player_max_points_earn);
                    int maxPointsEarn = b.getInt("maxPointsEarn");
                    maxPointsEarnTV.setText(Integer.toString(maxPointsEarn));
                } else if(b.getInt("number") == 88) {
                    if(b.getBoolean("status")) {
                        TextView nameTV = (TextView) findViewById(R.id.player_name);
                        nameTV.setText(b.getString("newName"));
                    }
                }
            }
        };
        b.putBoolean("type", true);
        activityMessenger = new Messenger(handler);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void changeName(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Change name");
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = layoutInflater.inflate(R.layout.dialog_change_name, null);
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Bundle b = new Bundle();
                b.putInt("number", 87);
                EditText nameET = (EditText) dialogView.findViewById(R.id.player_name_change);
                b.putString("extra", nameET.getText().toString());
                Message msg = Message.obtain();
                msg.setData(b);
                try {
                    connectionManagerMessenger.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton(R.string.dialog_button_cancel, null);
        builder.show();
    }
}
