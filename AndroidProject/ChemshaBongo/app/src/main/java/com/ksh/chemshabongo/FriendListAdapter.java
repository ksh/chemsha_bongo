package com.ksh.chemshabongo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class FriendListAdapter extends BaseAdapter {

    private Activity friendListActivity;
    private Messenger connectionManagerMessenger;
    private LayoutInflater layoutInflater;
    private ArrayList<Friend> friendList = new ArrayList<>();

    public FriendListAdapter(Activity friendListActivity, Messenger connectionManagerMessenger) {
        this.friendListActivity = friendListActivity;
        this.connectionManagerMessenger = connectionManagerMessenger;
        layoutInflater = (LayoutInflater) friendListActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return friendList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = layoutInflater.inflate(R.layout.friend_list_row, null);

        final TextView textView = (TextView) view.findViewById(R.id.friend_name);
        textView.setText(friendList.get(i).getName());

        final ImageButton removeButton = (ImageButton) view.findViewById(R.id.remove);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putInt("number", 108);
                b.putString("extra", Integer.toString(friendList.get(i).getId()));
                Message msg = Message.obtain();
                msg.setData(b);
                try {
                    connectionManagerMessenger.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    public void clearList() {
        friendList.clear();
        notifyDataSetChanged();
    }

    public void addFriend(Friend friend) {
        friendList.add(friend);
        notifyDataSetChanged();
    }

    public void removeFriend(int friendID) {
        for(int i = 0; i < friendList.size(); i++) {
            if(friendList.get(i).getId() == friendID) {
                friendList.remove(i);
                notifyDataSetChanged();
            }
        }
    }
}
