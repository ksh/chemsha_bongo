package com.ksh.chemshabongo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class FriendRequestListAdapter extends BaseAdapter {

    private Activity friendRequestActivity;
    private Messenger connectionManagerMessenger;
    private LayoutInflater layoutInflater;
    private ArrayList<Friend> friendRequestList = new ArrayList<>();

    public FriendRequestListAdapter(Activity friendRequestActivity, Messenger connectionManagerMessenger) {
        this.friendRequestActivity = friendRequestActivity;
        this.connectionManagerMessenger = connectionManagerMessenger;
        layoutInflater = (LayoutInflater) friendRequestActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return friendRequestList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = layoutInflater.inflate(R.layout.friend_request_list_row, null);

        final TextView textView = (TextView) view.findViewById(R.id.friend_name);
        textView.setText(friendRequestList.get(i).getName());
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(friendRequestActivity);
                builder.setTitle("Friend request");
                builder.setMessage("Do you want to add " + friendRequestList.get(i).getName() + " to your friends?");
                builder.setPositiveButton(R.string.dialog_accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        Bundle sendBundle = new Bundle();
                        sendBundle.putInt("number", 101);
                        sendBundle.putString("extra", Integer.toString(friendRequestList.get(i).getId()) + ";1");
                        Message msg = Message.obtain();
                        msg.setData(sendBundle);
                        try {
                            connectionManagerMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        friendRequestList.remove(i);
                        notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton(R.string.dialog_decline, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        Bundle sendBundle = new Bundle();
                        sendBundle.putInt("number", 101);
                        sendBundle.putString("extra", Integer.toString(friendRequestList.get(i).getId()) + ";0");
                        Message msg = Message.obtain();
                        msg.setData(sendBundle);
                        try {
                            connectionManagerMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        friendRequestList.remove(i);
                        notifyDataSetChanged();
                    }
                });
                builder.setNeutralButton(R.string.dialog_choose_later, null);
                builder.show();
            }
        });

        return view;
    }

    public void addFriend(Friend friend) {
        friendRequestList.add(friend);
        notifyDataSetChanged();
    }
}
