package com.ksh.chemshabongo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class OnlineFriendListAdapter extends BaseAdapter{

    CreateRoomActivity createRoomActivity;
    LayoutInflater layoutInflater;
    ArrayList<Friend> onlineFriendList = new ArrayList<>();

    public OnlineFriendListAdapter(CreateRoomActivity createRoomActivity) {
        this.createRoomActivity = createRoomActivity;
        layoutInflater = (LayoutInflater) createRoomActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return onlineFriendList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = layoutInflater.inflate(R.layout.online_friend_list_row, null);

        final TextView textView = (TextView) view.findViewById(R.id.friend_name);
        textView.setText(onlineFriendList.get(i).getName());

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createRoomActivity.inviteFriend(onlineFriendList.get(i).getId());
            }
        });

        return view;
    }

    public void addFriend(Friend friend) {
        onlineFriendList.add(friend);
        notifyDataSetChanged();
    }
}
