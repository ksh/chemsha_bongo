package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class RankingActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    Messenger connectionManagerMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        final RankingAdapter rankingAdapter = new RankingAdapter(this);
        ListView listView = (ListView) findViewById(R.id.ranking_list);
        listView.setAdapter(rankingAdapter);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");

        Bundle b = new Bundle();
        b.putInt("number", 106);
        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, RankingActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();

                if(b.getInt("number") == 107) {
                    int numberOfPlayers = b.getInt("numberOfPlayers");
                    for (int i = 0; i < numberOfPlayers; i++) {
                        int ranking = b.getInt("player" + Integer.toString(i) + "Ranking");
                        String name = b.getString("player" + Integer.toString(i) + "Name");
                        int points = b.getInt("player" + Integer.toString(i) + "Points");
                        RankingPlayer rankingPlayer = new RankingPlayer(ranking, name, points);
                        rankingAdapter.addRankingPlayer(rankingPlayer);
                    }
                }
            }
        };
        b.putBoolean("type", true);
        activityMessenger = new Messenger(handler);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
