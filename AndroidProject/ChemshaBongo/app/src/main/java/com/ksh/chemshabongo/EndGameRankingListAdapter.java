package com.ksh.chemshabongo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class EndGameRankingListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<String> players = new ArrayList<>();

    public EndGameRankingListAdapter(Activity gameEndActivity) {
        layoutInflater = (LayoutInflater) gameEndActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = layoutInflater.inflate(R.layout.end_game_ranking_list_row, null);

        final TextView textView = (TextView) view.findViewById(R.id.player_name);
        textView.setText(players.get(i));

        return view;
    }

    public void addPlayer(String player) {
        players.add(player);
        notifyDataSetChanged();
    }
}
