package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class OpponentsActivity extends AppCompatActivity {

    private Messenger connectionManagerMessenger;
    private Messenger activityMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");
        final int gameMode = intent.getIntExtra("gameMode", 0);
        final int bracketType = intent.getIntExtra("bracketType", 0);
        int numberOfPlayers = intent.getIntExtra("numberOfPlayers", 0);
        final String[] players = new String[numberOfPlayers];
        for(int i = 0; i < numberOfPlayers; i++) {
            players[i] = intent.getStringExtra("player" + Integer.toString(i) + "Name");
        }

        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, OpponentsActivity.this, connectionManagerMessenger);
            }
        };
        Bundle b = new Bundle();
        b.putBoolean("type", true);
        activityMessenger = new Messenger(handler);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        if(gameMode == 0) {
            setContentView(R.layout.activity_opponents);
            TextView playerNameTV = (TextView) findViewById(R.id.player_name);
            playerNameTV.setText(players[0]);
            TextView opponentNameTV = (TextView) findViewById(R.id.opponent_name);
            opponentNameTV.setText(players[1]);
        } else {
            if(bracketType == 1) {
                setContentView(R.layout.activity_opponents_tournament_4);

                TextView player1TV = (TextView) findViewById(R.id.player1);
                player1TV.setText(players[0]);

                TextView player2TV = (TextView) findViewById(R.id.player2);
                player2TV.setText(players[1]);

                TextView player3TV = (TextView) findViewById(R.id.player3);
                player3TV.setText(players[2]);

                TextView player4TV = (TextView) findViewById(R.id.player4);
                player4TV.setText(players[3]);

                if(numberOfPlayers >= 6) {
                    TextView player5TV = (TextView) findViewById(R.id.player5);
                    player5TV.setText(players[4]);

                    TextView player6TV = (TextView) findViewById(R.id.player6);
                    player6TV.setText(players[5]);
                }

                if(numberOfPlayers == 7) {
                    TextView player7TV = (TextView) findViewById(R.id.player7);
                    player7TV.setText(players[6]);
                }
            } else {
                setContentView(R.layout.activity_opponents_tournament_8);

                TextView player1TV = (TextView) findViewById(R.id.player1);
                player1TV.setText(players[0]);

                TextView player2TV = (TextView) findViewById(R.id.player2);
                player2TV.setText(players[1]);

                TextView player3TV = (TextView) findViewById(R.id.player3);
                player3TV.setText(players[2]);

                TextView player4TV = (TextView) findViewById(R.id.player4);
                player4TV.setText(players[3]);

                TextView player5TV = (TextView) findViewById(R.id.player5);
                player5TV.setText(players[4]);

                TextView player6TV = (TextView) findViewById(R.id.player6);
                player6TV.setText(players[5]);

                TextView player7TV = (TextView) findViewById(R.id.player7);
                player7TV.setText(players[6]);

                TextView player8TV = (TextView) findViewById(R.id.player8);
                player8TV.setText(players[7]);

                if(numberOfPlayers >= 12) {
                    TextView player9TV = (TextView) findViewById(R.id.player9);
                    player9TV.setText(players[8]);

                    TextView player10TV = (TextView) findViewById(R.id.player10);
                    player10TV.setText(players[9]);

                    TextView player11TV = (TextView) findViewById(R.id.player11);
                    player11TV.setText(players[10]);

                    TextView player12TV = (TextView) findViewById(R.id.player12);
                    player12TV.setText(players[11]);
                }

                if(numberOfPlayers >= 14) {
                    TextView player13TV = (TextView) findViewById(R.id.player13);
                    player13TV.setText(players[12]);

                    TextView player14TV = (TextView) findViewById(R.id.player14);
                    player14TV.setText(players[13]);
                }

                if(numberOfPlayers == 15) {
                    TextView player15TV = (TextView) findViewById(R.id.player15);
                    player15TV.setText(players[14]);
                }
            }
        }

        Handler postHandler = new Handler();
        postHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent gameplayIntent = new Intent(OpponentsActivity.this, GameplayActivity.class);
                gameplayIntent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
                startActivity(gameplayIntent);
                finish();
            }
        }, 3000);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
