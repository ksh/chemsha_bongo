package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

public class InviteFriendsActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    private Messenger connectionManagerMessenger;
    private InvitedFriendListAdapter invitedFriendListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        invitedFriendListAdapter = new InvitedFriendListAdapter(this);

        ListView friendList = (ListView) findViewById(R.id.invited_friend_list);
        friendList.setAdapter(invitedFriendListAdapter);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");

        Bundle b = new Bundle();
        b.putInt("number", 102);
        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, InviteFriendsActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();

                if(b.getInt("number") == 98 && b.getInt("status") == 2) {
                    int id = b.getInt("friendID");
                    String friendName = b.getString("friendName");
                    Friend friend = new Friend(id, friendName);
                    invitedFriendListAdapter.addFriend(friend);
                } else if(b.getInt("number") == 103) {
                    int numberOfFriends = b.getInt("numberOfInvitations");
                    for (int i = 0; i < numberOfFriends; i++) {
                        int id = b.getInt("invited" + Integer.toString(i) + "ID");
                        String name = b.getString("invited" + Integer.toString(i) + "Name");
                        Friend friend = new Friend(id, name);
                        invitedFriendListAdapter.addFriend(friend);
                    }
                }
            }
        };
        b.putBoolean("type", true);
        activityMessenger = new Messenger(handler);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void inviteFriend(View view) {
        Bundle b = new Bundle();
        b.putInt("number", 97);
        EditText friendNameET = (EditText) findViewById(R.id.input_player_name);
        final String friendName = friendNameET.getText().toString();
        b.putString("extra", friendName);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
