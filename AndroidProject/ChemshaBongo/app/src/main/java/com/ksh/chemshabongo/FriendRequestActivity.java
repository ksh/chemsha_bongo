package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class FriendRequestActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    private Messenger connectionManagerMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_requests);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");

        final FriendRequestListAdapter friendRequestListAdapter = new FriendRequestListAdapter(this, connectionManagerMessenger);
        ListView friendRequestList = (ListView) findViewById(R.id.friend_request_list);
        friendRequestList.setAdapter(friendRequestListAdapter);

        Bundle b = new Bundle();
        b.putInt("number", 104);
        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, FriendRequestActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();

                if(b.getInt("number") == 105) {
                    int numberOfFriends = b.getInt("numberOfInvitations");
                    for (int i = 0; i < numberOfFriends; i++) {
                        int id = b.getInt("invited" + Integer.toString(i) + "ID");
                        String name = b.getString("invited" + Integer.toString(i) + "Name");
                        Friend friend = new Friend(id, name);
                        friendRequestListAdapter.addFriend(friend);
                    }
                }
            }
        };
        b.putBoolean("type", true);
        activityMessenger = new Messenger(handler);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
