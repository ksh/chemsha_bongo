package com.ksh.chemshabongo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class RoomListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<String> roomList = new ArrayList<>();

    public RoomListAdapter(Activity activity) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return roomList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = layoutInflater.inflate(R.layout.room_list_row, null);

        final TextView textView = (TextView) view.findViewById(R.id.player_name);
        textView.setText(roomList.get(i));

        return view;
    }

    public void setList(ArrayList<String> list) {
        roomList.clear();
        roomList.addAll(list);
        notifyDataSetChanged();
    }
}
