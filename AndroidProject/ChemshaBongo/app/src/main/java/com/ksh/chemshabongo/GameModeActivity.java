package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class GameModeActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    Messenger connectionManagerMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_mode);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");

        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, GameModeActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();

                if(b.getInt("number") == 70) {
                    int roomID = b.getInt("roomID");
                    Intent intent = new Intent(GameModeActivity.this, CreateRoomActivity.class);
                    intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
                    intent.putExtra("roomID", roomID);
                    startActivity(intent);
                    finish();
                }
            }
        };
        activityMessenger = new Messenger(handler);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void playNormal(View view) {
        Bundle b = new Bundle();
        b.putInt("number", 69);
        b.putString("extra", "0");
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void playTournament(View view) {
        Bundle b = new Bundle();
        b.putInt("number", 69);
        b.putString("extra", "1");
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
