package com.ksh.chemshabongo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;

public class ConnectionManager extends Thread {

    public class Request {
        private int number;
        private String extraString;

        public Request(int number, String extraString) {
            this.number = number;
            this.extraString = extraString;
        }

        public int getNumber() {
            return number;
        }
        public String getExtraString() {
            return extraString;
        }
    }

    private MainActivity mainActivity;
    private Messenger activeMessenger;
    private int playerID;
    private final int protocolVersion = 20;
    private String serverIP = null;
    private final int serverPort = 1415;
    private Socket socket;
    private OutputStreamWriter writer;
    private BufferedReader reader;
    private boolean run = true;
    private LinkedList<Request> requests = new LinkedList<>();
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle b = msg.getData();
            if(b.getBoolean("type", false)) {
                activeMessenger = b.getParcelable("messenger");
            }
            if(b.getInt("number", 0) != 0) {
                Request request = new Request(b.getInt("number"), b.getString("extra"));
                requests.add(request);
            }
        }
    };

    public ConnectionManager(MainActivity mainActivity, Messenger messenger) {
        this.mainActivity = mainActivity;
        this.activeMessenger = messenger;
    }

    @Override
    public void run() {
        try {
            socket = new Socket(serverIP, serverPort);
            socket.setSoTimeout(1000);
            OutputStream outputStream = socket.getOutputStream();
            writer = new OutputStreamWriter(outputStream);
            InputStream inputStream = socket.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            reader = new BufferedReader(inputStreamReader);

            if(!connect()) {
                Bundle b = new Bundle();
                b.putInt("number", 66);
                b.putString("text","Unable to connect");
                Message msg = Message.obtain();
                msg.setData(b);
                try {
                    activeMessenger.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            while(run) {
                // Process incoming packet
                try {
                    String incomingPacket = reader.readLine();
                    if (incomingPacket != null) {
                        processIncomingPacket(incomingPacket);
                    }
                } catch (SocketTimeoutException | RemoteException e) {
                    e.printStackTrace();
                }

                // Send request
                Request request = requests.poll();
                if(request != null) {
                    try {
                        sendRequest(request);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean connect() throws IOException {
        SharedPreferences sharedPreferences = mainActivity.getPreferences(0);
        playerID = sharedPreferences.getInt("playerID", 0);

        // Connection REQ
        String msg = "A;";
        if (playerID > 0) {
            // connect to server
            msg += playerID;
        } else {
            // obtain player id
            msg += "0";
        }
        msg += ";" + protocolVersion + "\n";
        writer.write(msg);
        writer.flush();

        // Connection RESP
        String response = reader.readLine();
        String[] parts = response.split(";");
        if(!(parts.length == 3 || parts.length == 2) || !parts[0].equals("B") || parts[1].equals("0")) {
            return false;
        }
        if(parts[1].equals("1")) {
            /* To be deleted */
            Bundle b = new Bundle();
            b.putInt("comm", 1);
            b.putInt("playerID", playerID);
            Message message = Message.obtain();
            message.setData(b);
            try {
                activeMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            /**/
            return true;
        } else if(parts[1].equals("2") && parts.length == 3) {
            playerID = Integer.parseInt(parts[2]);
            SharedPreferences.Editor editor = mainActivity.getPreferences(0).edit();
            editor.putInt("playerID", playerID);
            editor.apply();

            /* To be deleted */
            Bundle b = new Bundle();
            b.putInt("comm", 1);
            b.putInt("playerID", playerID);
            Message message = Message.obtain();
            message.setData(b);
            try {
                activeMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            /**/
            return true;
        }
        return false;
    }
    private boolean processIncomingPacket(String packet) throws RemoteException {
        String[] parts = packet.split(";");
        if(parts.length <= 0) {
            return false;
        }
        switch (parts[0]) {
            case "F":
                // Room Reservation RESP
                if(parts.length == 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 70);
                    b.putInt("roomID", Integer.parseInt(parts[1]));
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "H":
                // Online Friend List RESP
                if(parts.length >= 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 72);
                    b.putInt("numberOfFriends", Integer.parseInt(parts[1]));
                    for (int i = 0; i < Integer.parseInt(parts[1]); i++) {
                        String[] elements = parts[2 + i].split(":");
                        if (elements.length != 2) {
                            return false;
                        }
                        b.putInt("friend" + Integer.toString(i) + "ID", Integer.parseInt(elements[0]));
                        b.putString("friend" + Integer.toString(i) + "Name", elements[1]);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "L":
                // Room state change
                if(parts.length >= 3) {
                    Bundle b = new Bundle();
                    b.putInt("number", 76);
                    b.putInt("numberOfPlayers", Integer.parseInt(parts[2]));
                    for (int i = 0; i < Integer.parseInt(parts[2]); i++) {
                        b.putString("player" + Integer.toString(i) + "Name", parts[3+i]);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "M":
                // Room Invitation REQ
                if(parts.length == 3) {
                    Bundle b = new Bundle();
                    b.putInt("number", 77);
                    b.putString("hostName", parts[1]);
                    b.putInt("roomID", Integer.parseInt(parts[2]));
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "P":
                // Game Start RESP
                if(parts.length >= 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 80);
                    boolean gameStartResponse = false;
                    if(parts[1].equals("1")) {
                        gameStartResponse = true;
                    }
                    b.putBoolean("gameStartResponse", gameStartResponse);
                    if(gameStartResponse && parts.length >= 7) {
                        b.putInt("gameMode", Integer.parseInt(parts[2]));
                        b.putInt("bracketType", Integer.parseInt(parts[3]));
                        b.putInt("numberOfPlayers", Integer.parseInt(parts[4]));
                        for (int i = 0; i < Integer.parseInt(parts[4]); i++) {
                            b.putString("player" + Integer.toString(i) + "Name", parts[5 + i]);
                        }
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "Q":
                // Question
                if(parts.length == 7) {
                    Bundle b = new Bundle();
                    b.putInt("number", 81);
                    b.putInt("questionID", Integer.parseInt(parts[1]));
                    b.putString("questionBody", parts[2]);
                    for(int i = 0; i < 4; i++) {
                        b.putString("answer" + Integer.toString(i), parts[3+i]);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "T":
                // Game End
                if(parts.length >= 3) {
                    Bundle b = new Bundle();
                    b.putInt("number", 84);
                    b.putString("pointsEarned", parts[1]);
                    b.putInt("numberOfPlayers", Integer.parseInt(parts[2]));
                    for(int i = 0; i < Integer.parseInt(parts[2]); i++) {
                        b.putString("player" + Integer.toString(i), parts[3+i]);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "V":
                // Profile RESP
                if(parts.length == 6) {
                    Bundle b = new Bundle();
                    b.putInt("number", 86);
                    b.putString("name", parts[1]);
                    b.putInt("points", Integer.parseInt(parts[2]));
                    b.putInt("ranking", Integer.parseInt(parts[3]));
                    b.putInt("gamesPlayed", Integer.parseInt(parts[4]));
                    b.putInt("maxPointsEarn", Integer.parseInt(parts[5]));
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "X":
                // Name Change RESP
                if(parts.length == 3) {
                    Bundle b = new Bundle();
                    b.putInt("number", 88);
                    if(parts[1].equals("1")) {
                        b.putBoolean("status", true);
                    } else {
                        b.putBoolean("status", false);
                    }
                    b.putString("newName", parts[2]);
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "Z":
                // Friend List RESP
                if(parts.length >= 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 90);
                    b.putInt("numberOfFriends", Integer.parseInt(parts[1]));
                    for(int i = 0; i < Integer.parseInt(parts[1]); i++) {
                        String[] elements = parts[2+i].split(":");
                        if(elements.length != 2) {
                            return false;
                        }
                        b.putInt("friend" + Integer.toString(i) + "ID", Integer.parseInt(elements[0]));
                        b.putString("friend" + Integer.toString(i) + "Name", elements[1]);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "b":
                // Invite Friend RESP
                if(parts.length == 4) {
                    Bundle b = new Bundle();
                    b.putInt("number", 98);
                    b.putInt("friendID", Integer.parseInt(parts[1]));
                    b.putString("friendName", parts[2]);
                    b.putInt("status", Integer.parseInt(parts[3]));
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "d":
                // Received Friend Invitation REQ
                if(parts.length == 3) {
                    Bundle b = new Bundle();
                    b.putInt("number", 100);
                    b.putInt("friendID", Integer.parseInt(parts[1]));
                    b.putString("friendName", parts[2]);
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "g":
                // Invitation List RESP
                if(parts.length >= 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 103);
                    b.putInt("numberOfInvitations", Integer.parseInt(parts[1]));
                    for(int i = 0; i < Integer.parseInt(parts[1]); i++) {
                        String[] elements = parts[2+i].split(":");
                        if(elements.length != 2) {
                            return false;
                        }
                        b.putInt("invited" + Integer.toString(i) + "ID", Integer.parseInt(elements[0]));
                        b.putString("invited" + Integer.toString(i) + "Name", elements[1]);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "i":
                // Received Friend Invitation List RESP
                if(parts.length >= 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 105);
                    b.putInt("numberOfInvitations", Integer.parseInt(parts[1]));
                    for(int i = 0; i < Integer.parseInt(parts[1]); i++) {
                        String[] elements = parts[2+i].split(":");
                        if(elements.length != 2) {
                            return false;
                        }
                        b.putInt("invited" + Integer.toString(i) + "ID", Integer.parseInt(elements[0]));
                        b.putString("invited" + Integer.toString(i) + "Name", elements[1]);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "k":
                // Ranking RESP
                if(parts.length >= 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 107);
                    b.putInt("numberOfPlayers", Integer.parseInt(parts[1]));
                    for(int i = 0; i < Integer.parseInt(parts[1]); i++) {
                        String[] elements = parts[2+i].split(":");
                        if(elements.length != 3) {
                            return false;
                        }
                        b.putString("player" + Integer.toString(i) + "Name", elements[0]);
                        b.putInt("player" + Integer.toString(i) + "Points", Integer.parseInt(elements[1]));
                        b.putInt("player" + Integer.toString(i) + "Ranking", Integer.parseInt(elements[2]));
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "m":
                // Remove Friend RESP
                if(parts.length == 3) {
                    Bundle b = new Bundle();
                    b.putInt("number", 109);
                    b.putInt("friendID", Integer.parseInt(parts[1]));
                    if(parts[2].equals("1")) {
                        b.putBoolean("status", true);
                    } else {
                        b.putBoolean("status", false);
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
            case "n":
                // Game State
                if(parts.length >= 2) {
                    Bundle b = new Bundle();
                    b.putInt("number", 110);
                    if(parts.length >= 6) {
                        b.putInt("gameMode", Integer.parseInt(parts[1]));
                        b.putInt("bracketType", Integer.parseInt(parts[2]));
                        b.putInt("numberOfPlayers", Integer.parseInt(parts[3]));
                        for (int i = 0; i < Integer.parseInt(parts[3]); i++) {
                            b.putString("player" + Integer.toString(i) + "Name", parts[4 + i]);
                        }
                    }
                    Message message = Message.obtain();
                    message.setData(b);
                    activeMessenger.send(message);
                    return true;
                }
                break;
        }
        return false;
    }
    private void sendRequest(Request request) throws IOException {
        String msg = Character.toString((char) request.getNumber());
        if(request.getNumber() == 67) {
            run = false;
        }
        if(request.getNumber() != 79) {
            msg += ";" + playerID;
        }
        String extra = request.getExtraString();
        if(extra != null) {
            msg += ";" + extra;
        }
        /* To be deleted */
        if(request.getNumber() == 1) {
            msg = request.getExtraString();
        }
        /**/
        msg += "\n";
        writer.write(msg);
        writer.flush();
    }

    public Messenger getMessenger() {
        return new Messenger(handler);
    }
    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }
}
