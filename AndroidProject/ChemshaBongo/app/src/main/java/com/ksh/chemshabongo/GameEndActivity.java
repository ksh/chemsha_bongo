package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

public class GameEndActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    private Messenger connectionManagerMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_end);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");
        Bundle intentBundle = intent.getBundleExtra("bundle");
        if(intentBundle.getInt("number") == 84) {
            TextView pointsEarnedTV = (TextView) findViewById(R.id.points_earned);
            pointsEarnedTV.setText(intentBundle.getString("pointsEarned"));

            ListView endGameRankingList = (ListView) findViewById(R.id.end_game_ranking_list);
            EndGameRankingListAdapter endGameRankingListAdapter = new EndGameRankingListAdapter(GameEndActivity.this);
            endGameRankingList.setAdapter(endGameRankingListAdapter);
            for(int i = 0; i < intentBundle.getInt("numberOfPlayers"); i++) {
                String playerName = intentBundle.getString("player" + Integer.toString(i));
                endGameRankingListAdapter.addPlayer(playerName);
            }
        }

        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, GameEndActivity.this, connectionManagerMessenger);

                final Bundle b = msg.getData();
                if(b.getInt("number") == 110) {
                    Intent opponentsIntent = new Intent(GameEndActivity.this, OpponentsActivity.class);
                    opponentsIntent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
                    opponentsIntent.putExtra("gameMode", b.getInt("gameMode"));
                    opponentsIntent.putExtra("bracketType", b.getInt("bracketType"));
                    int numberOfPlayers = b.getInt("numberOfPlayers");
                    opponentsIntent.putExtra("numberOfPlayers", numberOfPlayers);
                    for(int i = 0; i < numberOfPlayers; i++) {
                        opponentsIntent.putExtra("player" + Integer.toString(i) + "Name", b.getString("player" + Integer.toString(i) + "Name"));
                    }
                    startActivity(opponentsIntent);
                    finish();
                }
            }
        };
        activityMessenger = new Messenger(handler);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
