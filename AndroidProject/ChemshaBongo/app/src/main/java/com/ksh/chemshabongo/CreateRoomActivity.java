package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class CreateRoomActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    private Messenger connectionManagerMessenger;
    private int roomID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");
        roomID = intent.getIntExtra("roomID", 0);

        final OnlineFriendListAdapter onlineFriendListAdapter = new OnlineFriendListAdapter(this);
        ListView onlineFriendList = (ListView) findViewById(R.id.online_friend_list);
        onlineFriendList.setAdapter(onlineFriendListAdapter);

        final RoomListAdapter roomListAdapter = new RoomListAdapter(this);
        ListView roomList = (ListView) findViewById(R.id.room_list);
        roomList.setAdapter(roomListAdapter);

        Bundle b = new Bundle();
        if(!intent.getBooleanExtra("invited", false)) {
            b.putInt("number", 71);
        } else {
            findViewById(R.id.online_friend_list_title).setVisibility(View.GONE);
            findViewById(R.id.online_friend_list).setVisibility(View.GONE);
            findViewById(R.id.button_game_start).setVisibility(View.GONE);
        }
        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, CreateRoomActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();

                if(b.getInt("number") == 72) {
                    int numberOfFriends = b.getInt("numberOfFriends");
                    for (int i = 0; i < numberOfFriends; i++) {
                        int id = b.getInt("friend" + Integer.toString(i) + "ID");
                        String name = b.getString("friend" + Integer.toString(i) + "Name");
                        Friend friend = new Friend(id, name);
                        onlineFriendListAdapter.addFriend(friend);
                    }
                } else if(b.getInt("number") == 76) {
                    ArrayList<String> newRoomList = new ArrayList<>();
                    for(int i = 0; i < b.getInt("numberOfPlayers"); i++) {
                        newRoomList.add(b.getString("player" + Integer.toString(i) + "Name"));
                    }
                    roomListAdapter.setList(newRoomList);
                } else if(b.getInt("number") == 80) {
                    boolean gameStartResponse = b.getBoolean("gameStartResponse");
                    if(gameStartResponse) {
                        Intent opponentsIntent = new Intent(CreateRoomActivity.this, OpponentsActivity.class);
                        opponentsIntent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
                        opponentsIntent.putExtra("gameMode", b.getInt("gameMode"));
                        opponentsIntent.putExtra("bracketType", b.getInt("bracketType"));
                        int numberOfPlayers = b.getInt("numberOfPlayers");
                        opponentsIntent.putExtra("numberOfPlayers", numberOfPlayers);
                        for(int i = 0; i < numberOfPlayers; i++) {
                            opponentsIntent.putExtra("player" + Integer.toString(i) + "Name", b.getString("player" + Integer.toString(i) + "Name"));
                        }
                        startActivity(opponentsIntent);
                        finish();
                    }
                }
            }
        };
        b.putBoolean("type", true);
        activityMessenger = new Messenger(handler);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void inviteFriend(int friendID) {
        Bundle b = new Bundle();
        b.putInt("number", 75);
        String extra = Integer.toString(roomID) + ";" + Integer.toString(friendID);
        b.putString("extra", extra);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void gameStart(View view) {
        Bundle b = new Bundle();
        b.putInt("number", 79);
        String extra = Integer.toString(roomID);
        b.putString("extra", extra);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
