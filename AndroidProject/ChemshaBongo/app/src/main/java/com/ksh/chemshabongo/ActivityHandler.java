package com.ksh.chemshabongo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class ActivityHandler extends Handler {

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
    }

    public void handleCommonMessage(Message msg, final Activity activity, final Messenger connectionManagerMessenger) {
        final Bundle b = msg.getData();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        switch(b.getInt("number")) {
            case 68:
                Bundle bundle = new Bundle();
                bundle.putInt("number", 67);
                Message message = Message.obtain();
                try {
                    connectionManagerMessenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
                break;
            case 77:
                builder.setTitle("Room invitation");
                builder.setMessage("Player " + b.getString("hostName") + " invites you to join his room");
                builder.setPositiveButton(R.string.dialog_accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Bundle sendBundle = new Bundle();
                        sendBundle.putInt("number", 78);
                        sendBundle.putString("extra", Integer.toString(b.getInt("roomID")) + ";1");
                        Message msg = Message.obtain();
                        msg.setData(sendBundle);
                        try {
                            connectionManagerMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(activity, CreateRoomActivity.class);
                        intent.putExtra("invited", true);
                        intent.putExtra("roomID", b.getInt("roomID"));
                        intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
                        activity.startActivity(intent);
                    }
                });
                builder.setNegativeButton(R.string.dialog_decline, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Bundle sendBundle = new Bundle();
                        sendBundle.putInt("number", 78);
                        sendBundle.putString("extra", Integer.toString(b.getInt("roomID")) + ";0");
                        Message msg = Message.obtain();
                        msg.setData(sendBundle);
                        try {
                            connectionManagerMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.show();
                break;
            case 100:
                builder.setTitle("Friend request");
                builder.setMessage("Player " + b.getString("friendName") + " invites you to become his friend");
                builder.setPositiveButton(R.string.dialog_accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Bundle sendBundle = new Bundle();
                        sendBundle.putInt("number", 101);
                        sendBundle.putString("extra", Integer.toString(b.getInt("friendID")) + ";1");
                        Message msg = Message.obtain();
                        msg.setData(sendBundle);
                        try {
                            connectionManagerMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton(R.string.dialog_decline, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Bundle sendBundle = new Bundle();
                        sendBundle.putInt("number", 101);
                        sendBundle.putString("extra", Integer.toString(b.getInt("friendID")) + ";0");
                        Message msg = Message.obtain();
                        msg.setData(sendBundle);
                        try {
                            connectionManagerMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNeutralButton(R.string.dialog_choose_later, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Bundle sendBundle = new Bundle();
                        sendBundle.putInt("number", 101);
                        sendBundle.putString("extra", Integer.toString(b.getInt("friendID")) + ";2");
                        Message msg = Message.obtain();
                        msg.setData(sendBundle);
                        try {
                            connectionManagerMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.show();
                break;
        }
    }
}
