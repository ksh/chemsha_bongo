package com.ksh.chemshabongo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

public class FriendsActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    Messenger connectionManagerMessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");

        final FriendListAdapter friendListAdapter = new FriendListAdapter(this, connectionManagerMessenger);
        ListView friendList = (ListView) findViewById(R.id.friend_list);
        friendList.setAdapter(friendListAdapter);

        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, FriendsActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();

                if(b.getInt("number") == 90) {
                    friendListAdapter.clearList();
                    int numberOfFriends = b.getInt("numberOfFriends");
                    for (int i = 0; i < numberOfFriends; i++) {
                        int id = b.getInt("friend" + Integer.toString(i) + "ID");
                        String name = b.getString("friend" + Integer.toString(i) + "Name");
                        Friend friend = new Friend(id, name);
                        friendListAdapter.addFriend(friend);
                    }
                } else if(b.getInt("number") == 109) {
                    if(b.getBoolean("status")) {
                        friendListAdapter.removeFriend(b.getInt("friendID"));
                    }
                }
            }
        };
        activityMessenger = new Messenger(handler);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putInt("number", 89);
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void inviteFriends(View view) {
        Intent intent = new Intent(this, InviteFriendsActivity.class);
        intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
        startActivity(intent);
    }

    public void friendRequests(View view) {
        Intent intent = new Intent(this, FriendRequestActivity.class);
        intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
        startActivity(intent);
    }
}
