package com.ksh.chemshabongo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    private ConnectionManager connectionManager;
    private Messenger connectionManagerMessenger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activityMessenger = new Messenger(new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, MainActivity.this, connectionManagerMessenger);
                Bundle b = msg.getData();
                if(b.getInt("number") == 66) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(b.getString("text"));
                    builder.show();
                } else if(b.getInt("comm") == 1) {
                    TextView playerIDView = (TextView) findViewById(R.id.player_id);
                    playerIDView.setText("PlayerID: " + b.getInt("playerID"));
                }
            }
        });
        String serverIP = getPreferences(0).getString("serverIP", null);
        if(serverIP != null) {
            connectionManager = new ConnectionManager(MainActivity.this, activityMessenger);
            connectionManagerMessenger = connectionManager.getMessenger();
            connectionManager.setServerIP(serverIP);
            connectionManager.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(connectionManagerMessenger != null) {
            Bundle b = new Bundle();
            b.putBoolean("type", true);
            b.putParcelable("messenger", activityMessenger);
            Message msg = Message.obtain();
            msg.setData(b);
            try {
                connectionManagerMessenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(connectionManagerMessenger != null) {
            Bundle b = new Bundle();
            b.putInt("number", 67);
            Message msg = Message.obtain();
            msg.setData(b);
            try {
                connectionManagerMessenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_change_ip:
                changeIPDialog();
                break;
            // To be deleted
            case R.id.action_send_packet:
                sendPacketDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void play(View view) {
        if(connectionManagerMessenger != null) {
            Intent intent = new Intent(this, GameModeActivity.class);
            intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
            startActivity(intent);
        }
    }
    public void profile(View view) {
        if(connectionManagerMessenger != null) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
            startActivity(intent);
        }
    }
    public void friends(View view) {
        if(connectionManagerMessenger != null) {
            Intent intent = new Intent(this, FriendsActivity.class);
            intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
            startActivity(intent);
        }
    }
    public void ranking(View view) {
        if(connectionManagerMessenger != null) {
            Intent intent = new Intent(this, RankingActivity.class);
            intent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
            startActivity(intent);
        }
    }
    private void changeIPDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.change_ip);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = layoutInflater.inflate(R.layout.dialog_change_ip, null);
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText serverIPEditText = (EditText) dialogView.findViewById(R.id.ip);
                String serverIP = serverIPEditText.getText().toString();
                if(serverIP.equals("")) {
                    return;
                }
                
                if(connectionManagerMessenger != null) {
                    Bundle b = new Bundle();
                    b.putInt("number", 67);
                    Message msg = Message.obtain();
                    msg.setData(b);
                    try {
                        connectionManagerMessenger.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();

                    }
                }

                SharedPreferences.Editor editor = getPreferences(0).edit();
                editor.putString("serverIP", serverIP);
                editor.apply();
                

                connectionManager = new ConnectionManager(MainActivity.this, activityMessenger);
                connectionManagerMessenger = connectionManager.getMessenger();
                connectionManager.setServerIP(serverIP);
                connectionManager.start();
            }
        });
        builder.setNegativeButton(R.string.dialog_button_cancel, null);
        builder.show();
    }

    // To be deleted
    private void sendPacketDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.send_packet);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = layoutInflater.inflate(R.layout.dialog_change_name, null);
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText serverIPEditText = (EditText) dialogView.findViewById(R.id.player_name_change);
                String packetData = serverIPEditText.getText().toString();
                if(packetData.equals("")) {
                    return;
                }

                Bundle b = new Bundle();
                b.putInt("number", 1);
                b.putString("extra", packetData);
                Message msg = Message.obtain();
                msg.setData(b);
                try {
                    connectionManagerMessenger.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton(R.string.dialog_button_cancel, null);
        builder.show();
    }
}