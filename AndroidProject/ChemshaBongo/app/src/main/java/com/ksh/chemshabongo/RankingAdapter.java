package com.ksh.chemshabongo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class RankingAdapter extends BaseAdapter {

    Activity rankingActivity;
    LayoutInflater layoutInflater;
    ArrayList<RankingPlayer> rankingList = new ArrayList<>();

    public RankingAdapter(Activity rankingActivity) {
        this.rankingActivity = rankingActivity;
        layoutInflater = (LayoutInflater) rankingActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return rankingList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = layoutInflater.inflate(R.layout.ranking_list_row, null);

        RankingPlayer rankingPlayer = rankingList.get(i);

        TextView rankingTV = (TextView) view.findViewById(R.id.ranking);
        rankingTV.setText(Integer.toString(rankingPlayer.getRanking()));

        TextView nameTV = (TextView) view.findViewById(R.id.name);
        nameTV.setText(rankingPlayer.getName());

        TextView pointsTV = (TextView) view.findViewById(R.id.points);
        pointsTV.setText(Integer.toString(rankingPlayer.getPoints()));

        return view;
    }

    public void addRankingPlayer(RankingPlayer rankingPlayer) {
        rankingList.add(rankingPlayer);
        notifyDataSetChanged();
    }
}
