package com.ksh.chemshabongo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class GameplayActivity extends AppCompatActivity {

    private Messenger activityMessenger = null;
    private Messenger connectionManagerMessenger;
    private final int questionTime = 8000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameplay);

        Intent intent = getIntent();
        connectionManagerMessenger = intent.getParcelableExtra("connectionManagerMessenger");

        ActivityHandler handler = new ActivityHandler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                super.handleCommonMessage(msg, GameplayActivity.this, connectionManagerMessenger);
                final Bundle b = msg.getData();
                CountDownTimer timer = null;

                if(b.getInt("number") == 81) {
                    TextView questionTV = (TextView) findViewById(R.id.question);
                    final int questionID = b.getInt("questionID");
                    questionTV.setText(b.getString("questionBody"));

                    final TextView answer1TV = (TextView) findViewById(R.id.answer1);
                    final TextView answer2TV = (TextView) findViewById(R.id.answer2);
                    final TextView answer3TV = (TextView) findViewById(R.id.answer3);
                    final TextView answer4TV = (TextView) findViewById(R.id.answer4);

                    final ArrayList<TextView> answers = new ArrayList<>();
                    answers.add(answer1TV);
                    answers.add(answer2TV);
                    answers.add(answer3TV);
                    answers.add(answer4TV);
                    Collections.shuffle(answers);

                    final ProgressBar progressBar = (ProgressBar) findViewById(R.id.time);
                    progressBar.setMax(questionTime);
                    timer = new CountDownTimer(questionTime, 10) {
                        @Override
                        public void onTick(final long l) {
                            progressBar.setProgress((int) l);
                        }

                        @Override
                        public void onFinish() {
                            for (int i = 0; i < 4; i++) {
                                answers.get(i).setClickable(false);
                            }
                            Bundle b = new Bundle();
                            b.putInt("number", 82);
                            String extra = Integer.toString(questionID);
                            extra += ";0";
                            b.putString("extra", extra);
                            Message msg = Message.obtain();
                            msg.setData(b);
                            try {
                                connectionManagerMessenger.send(msg);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    final CountDownTimer finalTimer = timer;
                    View.OnClickListener onClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            for(int i = 0; i < 4 ; i++) {
                                answers.get(i).setClickable(false);
                            }
                            finalTimer.cancel();

                            Bundle bundle = new Bundle();
                            bundle.putInt("number", 82);
                            String extra = Integer.toString(questionID);
                            TextView textView = (TextView) view;
                            if(textView.getText().equals(b.getString("answer0"))) {
                                extra += ";1";
                                textView.setTextColor(Color.parseColor("#00FF00"));
                            } else {
                                extra += ";0";
                                textView.setTextColor(Color.parseColor("#FF0000"));
                            }
                            bundle.putString("extra", extra);
                            Message msg = Message.obtain();
                            msg.setData(bundle);
                            try {
                                connectionManagerMessenger.send(msg);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    for(int i = 0; i < 4; i++) {
                        answers.get(i).setText(b.getString("answer" + Integer.toString(i)));
                        answers.get(i).setTextColor(Color.parseColor("#000000"));
                        answers.get(i).setOnClickListener(onClickListener);
                        answers.get(i).setClickable(true);
                    }
                    timer.start();
                } else if(b.getInt("number") == 84) {
                    if (timer != null) {
                        timer.cancel();
                    }
                    Intent gameEndIntent = new Intent(GameplayActivity.this, GameEndActivity.class);
                    gameEndIntent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
                    gameEndIntent.putExtra("bundle", b);
                    startActivity(gameEndIntent);
                    finish();
                } else if(b.getInt("number") == 110) {
                    if (timer != null) {
                        timer.cancel();
                    }
                    Intent opponentsIntent = new Intent(GameplayActivity.this, OpponentsActivity.class);
                    opponentsIntent.putExtra("connectionManagerMessenger", connectionManagerMessenger);
                    opponentsIntent.putExtra("gameMode", b.getInt("gameMode"));
                    opponentsIntent.putExtra("bracketType", b.getInt("bracketType"));
                    int numberOfPlayers = b.getInt("numberOfPlayers");
                    opponentsIntent.putExtra("numberOfPlayers", numberOfPlayers);
                    for(int i = 0; i < numberOfPlayers; i++) {
                        opponentsIntent.putExtra("player" + Integer.toString(i) + "Name", b.getString("player" + Integer.toString(i) + "Name"));
                    }
                    startActivity(opponentsIntent);
                    finish();
                }
            }
        };
        activityMessenger = new Messenger(handler);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = new Bundle();
        b.putBoolean("type", true);
        b.putParcelable("messenger", activityMessenger);
        Message msg = Message.obtain();
        msg.setData(b);
        try {
            connectionManagerMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
