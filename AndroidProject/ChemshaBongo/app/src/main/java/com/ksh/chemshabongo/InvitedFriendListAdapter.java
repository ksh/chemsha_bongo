package com.ksh.chemshabongo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class InvitedFriendListAdapter extends BaseAdapter {
    Activity invitedFriendsActivity;
    LayoutInflater layoutInflater;
    ArrayList<Friend> invitedFriendList = new ArrayList<>();

    public InvitedFriendListAdapter(Activity invitedFriendsActivity) {
        this.invitedFriendsActivity = invitedFriendsActivity;
        layoutInflater = (LayoutInflater) invitedFriendsActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return invitedFriendList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = layoutInflater.inflate(R.layout.invited_friend_list_row, null);

        final TextView textView = (TextView) view.findViewById(R.id.friend_name);
        textView.setText(invitedFriendList.get(i).getName());

        return view;
    }

    public void addFriend(Friend friend) {
        invitedFriendList.add(friend);
        notifyDataSetChanged();
    }
}
